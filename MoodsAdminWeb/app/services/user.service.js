var app;
(function (app) {
    var services;
    (function (services) {
        "use strict";
        var UserService = (function () {
            function UserService($http, apiEndpoint) {
                this.$http = $http;
                this.apiEndpoint = apiEndpoint;
            }
            UserService.prototype.getById = function (id) {
                return this.$http.get(this.apiEndpoint.baseUrl + "/user/" + id)
                    .then(function (response) {
                    return response.data;
                });
            };
            UserService.$inject = ["$http", "app.blocks.ApiEndpoint"];
            return UserService;
        })();
        angular
            .module("app.services")
            .service('app.services.UserService', UserService);
    })(services = app.services || (app.services = {}));
})(app || (app = {}));
