module app.services {
  "use strict";

  export interface IUserService {
    getById(id: string): ng.IPromise<IUser>;
  }

  export interface IUser {
    id: string;
    name: string;
  }

  class UserService implements IUserService {

    static $inject=["$http","app.blocks.ApiEndpoint"];
    constructor(private $http: ng.IHttpService, private apiEndpoint: app.blocks.IApiEndpointConfig){
    }

      getById(id: string): ng.IPromise<IUser> {
        return this.$http.get(this.apiEndpoint.baseUrl +"/user/"+id)
          .then((response: ng.IHttpPromiseCallbackArg<IUser>): IUser => {
            return response.data;
            });
      }
    }

    angular
      .module("app.services")
      .service('app.services.UserService',UserService);

      /*
      * For creating Factory:
      factoryFunction.$inject = ["$http"];
      function factoryFunction($http: ng.IHttpService): IUserService {
        return new UserService($http);
      }

      angular.module("app.service")
             .factory("app.services.UserService", factoryFunction);
      */
  }
