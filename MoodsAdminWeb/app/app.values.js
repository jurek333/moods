(function () {
    "use strict";
    var currentUser = {};
    angular
        .module("app")
        .value("currentUser", currentUser);
})();
