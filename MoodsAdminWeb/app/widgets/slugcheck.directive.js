(function () {
    "use strict";
    angular
        .module("app.widgets")
        .directive("blSlugCheck", slugCheck);
    slugCheck.$inject = ["app.services.UserService"];
    function slugCheck(userService) {
        var directive = {
            restrict: 'A',
            link: link
        };
        function link(scope, element) {
            element.on("blur", function () {
                userService.getById("fsdfs").then(function (user) {
                    console.log(user.name);
                });
            });
        }
        return directive;
    }
})();
