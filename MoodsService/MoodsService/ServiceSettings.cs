﻿using System;
using System.Configuration;

namespace MoodsService
{
	public class SettingsFactory 
	{
		public static ServiceSettings CreateFromConfigFile() {
			return new ServiceSettings(){
				DbConnectigString = ConfigurationManager.ConnectionStrings["moodMySqlDb"].ConnectionString,
				Name = ConfigurationManager.AppSettings["ServiceName"],
				DisplayName = ConfigurationManager.AppSettings["ServiceDisplayName"],
				Description = ConfigurationManager.AppSettings["ServiceDescription"],
				Url = ConfigurationManager.AppSettings["Url"],
				Port = int.Parse(ConfigurationManager.AppSettings["Port"])
			};
		}
	}

	public class ServiceSettings
	{
		public string Name {get;set;}
		public string DisplayName {get;set;}
		public string Description {get;set;}
		public string Url {get;set;}
		public int Port {get;set;}
		public string DbConnectigString { get; set;}
	}
}

