﻿using System;
using Autofac;

namespace MoodsService
{
	public class SystemServiceContainerFactory
	{
		public static IContainer Create() {
			var builder = new ContainerBuilder();
			builder.RegisterType<MoodsService>().As<ISystemService>()
				.InstancePerDependency();
			builder.RegisterType<MySqlUserAverageMoodTable>().As<IUserAverageMoodStorage>()
				.InstancePerDependency();
			builder.RegisterType<MySqlDayAverageMoodTable>().As<IDayAverageMoodStorage>()
				.InstancePerDependency();
			builder.RegisterType<MySqlSprintTable> ().As<ISprintTableStorage> ()
				.InstancePerDependency ();
            builder.RegisterType<MySqlMoodTable>().As<IMoodStorage>()
                .InstancePerDependency();
            builder.Register (c => SettingsFactory.CreateFromConfigFile ()).As<ServiceSettings> ()
				.InstancePerLifetimeScope();
			builder.RegisterType<ProcessManagerActor> ().AsSelf ().InstancePerDependency ();
			builder.RegisterType<WebApiActor> ().AsSelf ().InstancePerDependency ();
			builder.RegisterType<UserAverageMoodActor> ().AsSelf ().InstancePerDependency ();
			builder.RegisterType<DayAverageMoodActor> ().AsSelf ().InstancePerDependency ();
			builder.RegisterType<DbCheckActor> ().AsSelf ().InstancePerDependency ();
			builder.RegisterType<DbConsistencyCheck> ().As<IDbCheck> ().InstancePerDependency ();
			var container = builder.Build ();
			return  container;
		}
	}
}

