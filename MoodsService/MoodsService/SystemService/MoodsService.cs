﻿using System;
using Akka.Actor;
using Akka.DI.AutoFac;

namespace MoodsService
{
	public class MoodsService : ISystemService
	{
		private ActorSystem _system;
		private ServiceSettings _settings;

		public MoodsService (ServiceSettings settings)
		{
			_settings = settings;
		}

		public void Start ()
		{
			_system = ActorSystem.Create (string.Concat (_settings.Name, "-MoodsGears"));

			// Create the ActorSystem and Dependency Resolver
			var propsResolver = new AutoFacDependencyResolver(SystemServiceContainerFactory.Create (), _system);

			_system.ActorOf(propsResolver.Create<ProcessManagerActor>(), ProcessManagerActor.ActorName);
		}

		public void Stop ()
		{
			Console.Write ("shutdown.....");
			_system.Shutdown ();
		}
	}
}

