using System;
using Akka.Actor;
using Nancy.Hosting.Self;
using Nancy.Bootstrapper;
using Akka.DI.Core;
using System.Linq;

namespace MoodsService
{
	public class UserAverageMoodActor : ReceiveActor
	{
		public static readonly string ActorName = "UserAverageMoodCalculator";
		private IUserAverageMoodStorage _storage;

		public UserAverageMoodActor(IUserAverageMoodStorage storage)
		{
			_storage = storage;

			Receive<MoodMsg> (async msg =>  {
				await _storage.AddMood (msg.GetData ());
				Self.Tell(PoisonPill.Instance, Sender);
			});

		}
	}
	public class DayAverageMoodActor : ReceiveActor
	{
		public static readonly string ActorName = "DayAverageMoodCalculator";
		private IDayAverageMoodStorage _storage;
        private IMoodStorage _moodHistory;

        public DayAverageMoodActor(IDayAverageMoodStorage storage, IMoodStorage moods)
		{
            _moodHistory = moods;
			_storage = storage;

			Receive<MoodMsg> (async msg =>  {
                var moodsHist = await _moodHistory.GetMoods(msg.Day, msg.Day);

                var meanResults = moodsHist.GroupBy(p => p.UserId,
                                p=>p.MoodValue,
                                (user, val) => new {
                                    UserId = user,
                                    Average = val.Average()
                                });

                DateTime day = msg.Day;
                double moodTotalSum = meanResults.Sum(p => p.Average);
                int moodCount = meanResults.Count();
                await _storage.UpdateAvgMood(day, moodCount, moodTotalSum);
				//await _storage.AddMood (msg.GetData ());
				Self.Tell(PoisonPill.Instance, Sender);
			});

		}
	}
}
