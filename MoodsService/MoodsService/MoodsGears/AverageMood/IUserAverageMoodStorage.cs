using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace MoodsService
{
	public interface IUserAverageMoodStorage
	{
		Task<UserAverage> GetAverage(int user);
		Task<int> AddMood(MoodData data);
	}
	
}