using System;

namespace MoodsService
{
	public class MoodOperationResponse 
	{
		public MoodData Mood{get;set;}
		public bool IsSuccess {get;set;}
	}
}
