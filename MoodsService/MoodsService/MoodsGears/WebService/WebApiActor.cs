using System;
using Akka.Actor;
using Nancy.Hosting.Self;
using Nancy.Bootstrapper;

namespace MoodsService
{
	public interface IActorPublicator {
		void PublicateMoodMsg (MoodData data);
	}

	public class WebApiActor : ReceiveActor,IActorPublicator
	{
		public static readonly string ActorName = "Nancy";

		private NancyHost _nancyHost;
		private ServiceSettings _settings;
		private ActorSelection _coordinator;

		public WebApiActor(ServiceSettings settings)
		{
			_settings = settings;
			_coordinator = Context.ActorSelection (Self.Path.Parent);
		}

		protected override void PreStart ()
		{
			base.PreStart ();
			_nancyHost = new NancyHost (
				new Uri (string.Concat (_settings.Url, ":", _settings.Port)), 
				new WebServiceBootStraper (this));
			_nancyHost.Start ();
		}

		public void PublicateMoodMsg(MoodData data){
			_coordinator.Tell (new MoodMsg (data), ActorRefs.NoSender);
		}

		protected override void PostStop ()
		{
			_nancyHost.Stop ();
			base.PostStop ();
		}
	}
	
}
