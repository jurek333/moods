﻿using System;
using Nancy;
using System.Threading.Tasks;
using Nancy.ModelBinding;
using System.Collections.Generic;

namespace MoodsService
{
	public class MoodModule : NancyModule
	{
		private IMoodStorage _storage;
		private IActorPublicator _publicator;
		public MoodModule (IMoodStorage storage, IActorPublicator publicator):base("/mood")
		{
			_storage = storage;
			_publicator = publicator;

			Post ["/", true] = async (p, ct) => await SaveUserMood ();
			Get ["/{userId}", true] = async (p, ct) => await GetUserMoods (p.userId);
			Get ["/{begin:datetime(yyyy-MM-dd)}/{end:datetime(yyyy-MM-dd)}", true] = async (p, ct) => 
				await GetMoods (p.begin, p.end);
		}

		private async Task<MoodOperationResponse> SaveUserMood() 
		{
			MoodData data = this.Bind<MoodData> ();
			int operationCount = await _storage.SaveMood (data);

			_publicator.PublicateMoodMsg (data);

			return new MoodOperationResponse {
				IsSuccess = operationCount != 0, 
				Mood = data
			};
		}

		private async Task<List<MoodData>> GetMoods (DateTime begin, DateTime end)
		{
			var mood = await _storage.GetMoods (begin,end);
			return mood;
		}

		private async Task<List<MoodData>> GetUserMoods(int user) 
		{
			var mood = await _storage.GetUserMood (user);
			return mood;
		}
	}
}

