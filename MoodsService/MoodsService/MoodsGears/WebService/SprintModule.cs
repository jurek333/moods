using System;
using Nancy;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Nancy.ModelBinding;

namespace MoodsService
{
	public class SprintModule : NancyModule
	{
		private readonly IDayAverageMoodStorage _dayStorage;
		private readonly ISprintTableStorage _sprintStorage;

		public SprintModule (IDayAverageMoodStorage dayStorage, 
			ISprintTableStorage sprintStorage)
			:base("/sprint")
		{
			_dayStorage = dayStorage;
			_sprintStorage = sprintStorage;

            Get["/{no:long}", true] = async(p, ct) => {
				if (null == p.no && p.no < 1)
					return Negotiate.WithStatusCode (HttpStatusCode.BadRequest);
				return await SprintAverage(p.no);
			};

			Post ["/start-new", true] = async(p, ct) => {
				SprintMood sp = this.Bind<SprintMood>();
				return await StartNewOne(sp.Start, sp.Name);
			};

			Post["/{no:long}/change-name", true] = async(p, ct) => {
				if (null == p.no && p.no < 1)
					return Negotiate.WithStatusCode (HttpStatusCode.BadRequest);
				SprintMood sp = this.Bind<SprintMood>();
				int operation = await _sprintStorage.ChangeName(p.no, sp.Name);

				if((int)DbOperationStatus.Ok == operation)
					return true;
				return false;
			};
				
			Post ["/{no:long}/end", true] = async(p, ct) => {
				if (null == p.no && p.no < 1)
					return Negotiate.WithStatusCode (HttpStatusCode.BadRequest);
				DateTime lastDay;
				if (false == DateTime.TryParse (this.Request.Query ["lastDay"], out lastDay))
					lastDay = DateTime.Today;

				return await EndSprint (p.no, lastDay);
			};
		}

		private async Task<Sprint> StartNewOne (DateTime startDay, string name)
		{
			Sprint sprint = new Sprint () {
				Start = startDay,
				Name = name
			};
			sprint.SprntNo = await _sprintStorage.StartNew (sprint);
			return sprint;
		}

		private async Task<bool> EndSprint (long no, DateTime lastDay)
		{
			var result = await _sprintStorage.EndOne (no, lastDay);
			if (result == (int)DbOperationStatus.Ok)
				return true;
			return false;
		}

		private async Task<SprintMood> SprintAverage (long sprintNo)
		{
			var sprint = await _sprintStorage.GetSprint (sprintNo);
			if (null == sprint)
				return null;

            SprintMood result = new SprintMood(sprint);
			var stats = await _dayStorage.GetAverage (sprint.Start, sprint.End ?? DateTime.Today);
			int days = stats.Count;
			float avg = stats.Sum (s => s.Average) / (float)days;
			result.Average = avg;
			return result;
		}
	}
}