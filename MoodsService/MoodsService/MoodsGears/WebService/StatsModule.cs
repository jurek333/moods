using System;
using Nancy;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace MoodsService
{
	public class StatsModule : NancyModule
	{
		private IDayAverageMoodStorage _dayStorage;
		private IUserAverageMoodStorage _userStorage;

		public StatsModule (IDayAverageMoodStorage dayStorage, IUserAverageMoodStorage userStorage)
			:base("/stats")
		{
			_dayStorage = dayStorage;
			_userStorage = userStorage;

			Get["/user/{user:int}", true] = async(p, ct) => {
				return await UserAverage(p.user);
			};
				
			Get ["/days", true] = async(p, ct) => {
				DateTime since, until;
				if (false == DateTime.TryParse (this.Request.Query ["since"], out since))
					return Negotiate.WithStatusCode (HttpStatusCode.BadRequest);
				if (false == DateTime.TryParse (this.Request.Query ["until"], out until))
					until = since;

				return await DayAverage (since, until);
			};
		}

		private async Task<UserAverage> UserAverage (int user)
		{
			var mood = await _userStorage.GetAverage(user);
			return mood;
		}

		private async Task<List<DayAverage>> DayAverage (DateTime since, DateTime until)
		{
			var mood = await _dayStorage.GetAverage(since, until);
			return mood;
		}
	}
}   