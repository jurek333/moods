﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MoodsService
{
	public interface IMoodStorage 
	{
		Task<int> SaveMood(MoodData data);
		Task<List<MoodData>> GetMoods (DateTime begin, DateTime end);
		Task<List<MoodData>> GetUserMood (int user);
	}
}

