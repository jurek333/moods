using System;

namespace MoodsService
{

	public class DayAverage 
	{
		public DateTime Day { get; set; }
		public float Average { get; set; }
	}
    
    public class SprintAverage 
    {
        public int SprntNo { get; set; }
        public float Average {get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Name { get; set; }
    }
}
