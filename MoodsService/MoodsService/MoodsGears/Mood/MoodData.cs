﻿using System;

namespace MoodsService
{
	public class MoodData
	{
		public int UserId { get; set; }
		public DateTime Day { get; set; }
		public int MoodValue { get; set; }

		public DateTime Timestamp { get; set;}

		public MoodData ()
		{
			Timestamp = DateTime.UtcNow;
		}
	}
}


