using System;

namespace MoodsService
{

	public class UserAverage {
		public int UserId {get;set;}
		public float Average { get; set; }
	}

}
