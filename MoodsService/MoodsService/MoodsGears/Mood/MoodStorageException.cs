using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MoodsService
{
	public class MoodStorageException : ApplicationException
	{
		public MoodStorageException (string msg) : base(msg) {}
		public MoodStorageException (string msg, Exception ex) : base(msg, ex) {}
	}
	
}
