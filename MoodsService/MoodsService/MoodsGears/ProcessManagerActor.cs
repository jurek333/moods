﻿using System;
using Akka.Actor;
using Nancy.Hosting.Self;
using Nancy.Bootstrapper;
using Akka.DI.Core;

namespace MoodsService
{

	public class ProcessManagerActor : ReceiveActor
	{
		public static readonly string ActorName = "Boss";

		public ProcessManagerActor()
		{
			Receive<MoodMsg> (msg => {
				var userAvg = Context.DI ().ActorOf<UserAverageMoodActor> (UserAverageMoodActor.ActorName);
				userAvg.Tell (msg);
				var dayAvg = Context.DI ().ActorOf<DayAverageMoodActor> (DayAverageMoodActor.ActorName);
				dayAvg.Tell (msg);
			});
		}

		private void CheckingDb() 
		{
			Receive<DbCheckingMsg> (msg=>{
				UnbecomeStacked ();

				if(msg.DbIsOk) 
					Context.DI ().ActorOf<WebApiActor> (WebApiActor.ActorName);
				else 
					Self.GracefulStop(TimeSpan.FromSeconds(5));
			});
		}

		protected override void PreStart ()
		{
			BecomeStacked(CheckingDb);

			Context.DI ().ActorOf<DbCheckActor> (DbCheckActor.ActorName);
		}

		protected override void PostRestart (Exception reason)
		{
			Context.DI ().ActorOf<WebApiActor> (WebApiActor.ActorName);
		}

		protected override SupervisorStrategy SupervisorStrategy()
		{
			return new OneForOneStrategy(
				10,
				TimeSpan.FromSeconds(30),
				x =>
				{
					//Maybe we consider ArithmeticException to not be application critical
					//so we just ignore the error and keep going.
					if (x is ArithmeticException) return Directive.Resume;

					//Error that we cannot recover from, stop the failing actor
					else if (x is NotSupportedException) return Directive.Stop;

					//In all other cases, just restart the failing actor
					else return Directive.Restart;
				});
		}
	}
}

