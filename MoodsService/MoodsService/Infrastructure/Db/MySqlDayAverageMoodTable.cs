using System;
using System.Data;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;
using System.Collections.Generic;
using Dapper;
using System.Linq;

namespace MoodsService
{

	public class MySqlDayAverageMoodTable : MySqlTableStorage, IDayAverageMoodStorage
	{
		public MySqlDayAverageMoodTable (ServiceSettings settings):base(settings)
		{
		}

		public async Task<List<DayAverage>> GetAverage(DateTime since, DateTime until) 
		{
			var result = await _connection.QueryAsync<dynamic> (
				"select Day, MoodCounter, MoodTotalSum " +
				"from DayAverageMood " +
				"where Day >= @Since AND Day <= @Until; ", 
				new {Since = since, Until = until});
			List<DayAverage> moods = new List<DayAverage>();
			foreach(var item in result)
			{
				moods.Add(new DayAverage(){
					Day = item.Day, 
					Average = item.MoodTotalSum/(float)item.MoodCounter
				});
			}
			return moods;
		}

        //public async Task<int> AddMood (MoodData data)
        //{
        //    return await _connection.ExecuteAsync (
        //        "INSERT INTO DayAverageMood(Day, MoodCounter, MoodTotalSum) " +
        //        "VALUES( @Day, 1, @Mood) " +
        //        "ON DUPLICATE KEY " +
        //        "UPDATE " +
        //        "    MoodCounter = MoodCounter + 1, " +
        //        "    MoodTotalSum = MoodTotalSum + @Mood; ",
        //        new {Day = data.Day, Mood = data.MoodValue});
        //}

        public async Task<int> UpdateAvgMood(DateTime day, int userCount, double moodSum)
        {
            return await _connection.ExecuteAsync(
                "INSERT INTO DayAverageMood(Day, MoodCounter, MoodTotalSum) " +
                "VALUES( @Day, @Count, @MoodSum) " +
                "ON DUPLICATE KEY " +
                "UPDATE " +
                "    MoodCounter = @Count, " +
                "    MoodTotalSum = @MoodSum; ",
                new { Day = day, Count = userCount, MoodSum = moodSum });
        }
    }

}
