using System;
using System.Data;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;
using System.Collections.Generic;
using Dapper;
using System.Linq;

namespace MoodsService
{
    public class MoodUser
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string PhotoUrl { get; set; }
        public int? Team {get;set;}
    }
    public interface IMoodUsersTableStorage
    {
        Task<MoodUser> User(int user);
        Task<List<MoodUser>> Users(int? team);
    }
    public class MySqlMoodUsersTable : MySqlTableStorage, IMoodUsersTableStorage
    {
        public MySqlMoodUsersTable(ServiceSettings settings) : base(settings)
        {
        }
        public async Task<MoodUser> User(int user)
        {
            var result = await _connection.QueryAsync<MoodUser>(
                "select UserId, Name, PhotoUrl " +
                "from MoodUsers where UserId = @Id; ",
                new { Id = user });
            return result.FirstOrDefault();
        }
        
        public async Task<List<MoodUser>> Users(int? team)
        {
            var result = await _connection.QueryAsync<MoodUser>(
                "select UserId, Name, PhotoUrl " +
                "from MoodUsers " +
                (team.HasValue? "where Team = @TeamId": ";"),
                new { TeamId = team });
            return result.ToList();
        }
    }
}
