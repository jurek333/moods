using System;
using System.Data;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;
using System.Collections.Generic;
using Dapper;
using System.Linq;

namespace MoodsService
{
	public interface ISprintTableStorage
	{
		Task<Sprint> GetSprint (long sprintNo);
		Task<int> ChangeName(long sprintNo, string newName);
		Task<long> StartNew (Sprint newSprint);
		Task<int> EndOne (long sprintNo, DateTime lastDay);
	}

	public class MySqlSprintTable : MySqlTableStorage, ISprintTableStorage
	{
		public MySqlSprintTable (ServiceSettings settings):base(settings)
		{
		}
        
		public async Task<Sprint> GetSprint(long sprintNo) 
		{
			if (sprintNo < 1)
				throw new ArgumentOutOfRangeException ("sprintNo", 
					"parametr nie może być 0 lub liczbą ujemną");
			
			var result = await _connection.QueryAsync<Sprint> (
				"select SprintNo, Start, End, Name " +
				"from Sprints " +
				"where SprintNo = @No; ", 
				new {No = sprintNo});
			
			var sprint = result.FirstOrDefault();
			return sprint;
		}

		public async Task<int> ChangeName(long sprintNo, string newName)
		{
			if (sprintNo < 1)
				throw new ArgumentOutOfRangeException ("sprintNo", 
					"parametr nie może być 0 lub liczbą ujemną");
			if (!string.IsNullOrEmpty (newName) && newName.Length > 400)
				return (int)DbOperationStatus.NameToLong;
		
			int result = await _connection.ExecuteAsync (
				             "update Sprints set Name = @NewName where SprintNo = @No",
				             new {NewName = newName, No = sprintNo});
			if (0 < result)
				return (int)DbOperationStatus.Ok;
			else if (0 == result)
				return (int)DbOperationStatus.ObjectDontExists;
			return (int)DbOperationStatus.UnknowError;
		}

		public async Task<long> StartNew(Sprint newSprint)
		{
			if (null == newSprint)
				throw new NullReferenceException ("newSprint");
			if (!string.IsNullOrEmpty (newSprint.Name) && newSprint.Name.Length > 400)
				return (int)DbOperationStatus.NameToLong;
			
			const string query = "insert into Sprints (Start, Name) values (@Start, @End);"
			                     + "select cast(scope_identity() as int)";
			var res = await _connection.QueryAsync<long> (query, newSprint);
			return res.FirstOrDefault ();
		}

		public async Task<int> EndOne (long sprintNo, DateTime lastDay)
		{
			if (sprintNo < 1)
				throw new ArgumentOutOfRangeException ("sprintNo", 
					"parametr nie może być 0 lub liczbą ujemną");

			int result = await _connection.ExecuteAsync (
				"update Sprints set End=@LastDay where SprintNo = @No;",
				new {LastDay = lastDay, No = sprintNo});
			if (0 < result)
				return (int)DbOperationStatus.Ok;
			else if (0 == result)
				return (int)DbOperationStatus.ObjectDontExists;
			
			return (int)DbOperationStatus.UnknowError;
		}
	}
}
