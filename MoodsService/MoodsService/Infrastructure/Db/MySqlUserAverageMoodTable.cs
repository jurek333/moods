using System;
using System.Data;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;
using System.Collections.Generic;
using Dapper;
using System.Linq;

namespace MoodsService
{
	public class MySqlUserAverageMoodTable : MySqlTableStorage, IUserAverageMoodStorage
	{
		public MySqlUserAverageMoodTable (ServiceSettings settings):base(settings)
		{
		}


		public async Task<UserAverage> GetAverage(int user) 
		{
			var result = await _connection.QueryAsync<dynamic> (
				"select UserId, MoodCounter, MoodTotalSum " +
				"from UserAverageMood " +
				"where UserId = @User; ", 
				new {User = user});
			var res = result.FirstOrDefault();
			var avg = new UserAverage(){
				UserId = user
			};
			if(null != res)
				avg.Average = res.MoodTotalSum/(float)res.MoodCounter;

			return avg;
		}

		public async Task<int> AddMood (MoodData data)
		{
			return await _connection.ExecuteAsync (
				"INSERT INTO UserAverageMood(UserId, MoodCounter, MoodTotalSum) " +
				"VALUES( @UserId, 1, @Mood) " +
				"ON DUPLICATE KEY " +
				"UPDATE " +
				"    MoodCounter = MoodCounter + 1, " +
				"    MoodTotalSum = MoodTotalSum + @Mood; ",
				new {UserId = data.UserId, Mood = data.MoodValue});
		}
	}
}
