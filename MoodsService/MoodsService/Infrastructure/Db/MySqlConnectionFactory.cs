﻿using System;
using System.Data;
using MySql.Data.MySqlClient;

namespace MoodsService
{
	public interface IDbConnectionFactory {
		IDbConnection Create(string connectionString);
	}

	public class MySqlConnectionFactory:IDbConnectionFactory
	{
		public IDbConnection Create (string connectionString)
		{
			MySqlConnection conn = new MySqlConnection (connectionString);
			return conn;
		}
	}

	public abstract class MySqlTableStorage : IDisposable
	{
		protected IDbConnection _connection;

		public MySqlTableStorage (ServiceSettings settings)
		{
			_connection = new MySqlConnectionFactory ().Create (settings.DbConnectigString);
			_connection.Open ();
		}

		public void Dispose ()
		{
			_connection.Close ();
		}
	}
}

