﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;
using System.Collections.Generic;
using Dapper;
using System.Linq;

namespace MoodsService
{

	public class MySqlMoodTable : MySqlTableStorage, IMoodStorage
	{
		public MySqlMoodTable (ServiceSettings settings):base(settings)
		{
		}

		public async Task<int> SaveMood (MoodData data)
		{
			return await _connection.ExecuteAsync (
				"insert into Moods(UserId, Day, MoodValue, Timestamp) " +
				"values(@UserId, @Day, @MoodValue, @Timestamp) ;", 
				new {UserId = data.UserId, Day = data.Day, 
					MoodValue = data.MoodValue, Timestamp = data.Timestamp}
			);
		}
			
		public async Task<List<MoodData>> GetMoods (DateTime begin, DateTime end)
		{
			var moods = await _connection.QueryAsync<MoodData> (
				"select UserId, Day, MoodValue, Timestamp " +
				"from Moods where Day>= @Start and Day <= @End  ;", 
				new {Start = begin, End = end});
			return moods.ToList();
		}

		public async Task<List<MoodData>> GetUserMood (int user)
		{
			var moods = await _connection.QueryAsync<MoodData>(
				"select UserId, Day, MoodValue, Timestamp " +
				"from Moods where UserId = @Param; ", new { Param = user});
			return moods.ToList();
		}
	}
}

