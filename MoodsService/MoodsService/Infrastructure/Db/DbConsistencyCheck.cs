using System;
using System.Data;
using System.Linq;
using Dapper;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace MoodsService
{

	public class DbConsistencyCheck: IDbCheck, IDisposable
	{
		private IDbConnection _connection;

		public DbConsistencyCheck (ServiceSettings settings)
		{
			_connection = new MySqlConnectionFactory ().Create (settings.DbConnectigString);
			_connection.Open ();
		}

		/*private string _queryCheckDbExists = "SELECT SCHEMA_NAME " +
			"FROM INFORMATION_SCHEMA.SCHEMATA " +
			"WHERE SCHEMA_NAME = 'MoodsDb';";*/

		private string _queryCheckTableExists = string.Concat("SELECT ",
			"table_name " ,
			"FROM information_schema.tables " ,
			"WHERE table_schema = @DbName AND table_name in @TableNames; ");

		private string _dbName = "MoodsDb";
		private Dictionary<string, string> _tableNames = new Dictionary<string,string>(){
			{"Moods", "create table Moods(UserId int UNSIGNED not null, Day datetime not null, MoodValue int not null, Timestamp datetime not null);" +
				"ALTER TABLE `Moods` ADD INDEX `IDX_UserId` (`UserId`); " +
				"ALTER TABLE `Moods` ADD INDEX `IDX_Day` (`Day`) "},
			{"UserAverageMood","create table UserAverageMood(" +
				"UserId int UNSIGNED not null PRIMARY KEY, " +
				"MoodCounter int not null, " +
				"MoodTotalSum int not null);"},
			{"DayAverageMood","create table DayAverageMood(" +
				"Day datetime not null PRIMARY KEY, " +
				"MoodCounter int not null, " +
				"MoodTotalSum float not null);"},
			{"MoodUsers", "create table MoodUsers( "+
				"UserId int unsigned not null PRIMARY KEY, "+
				"Name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci, " +
				"PhotoUrl VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci, "+
				"Team int unsigned null );"},
            {"Sprints", "create table Sprints( "
				+"SprintNo bigint UNSIGNED not null AUTO_INCREMENT, "
                +"Start datetime not null, "
                +"End datetime null, "
				+"Name VARCHAR(401) CHARACTER SET utf8 COLLATE utf8_unicode_ci, "
				+"PRIMARY KEY(SprintNo));"}
		};

		public bool CheckTables()
		{
			var queryParams = _tableNames.Keys.ToList();
				
			var tables = _connection.Query<string> (_queryCheckTableExists, 
				             new{DbName = _dbName, TableNames = queryParams }).ToList ();
			foreach (string key in _tableNames.Keys) {
				if (tables.Contains (key))
					continue;

				string query = _tableNames [key];
				_connection.Execute (query);
			}

			return true;
		}

		public void Dispose ()
		{
			_connection.Close ();
		}
	}
}
