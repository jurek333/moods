'use strict';

/**
 * @ngdoc overview
 * @name moodsWebApp
 * @description
 * # moodsWebApp
 *
 * Main module of the application.
 */
angular
  .module('moodsWebApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui-notification',
    'moodsWebApp.config'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'users/users.html',
        controller: 'UsersCtrl'
      })
      .when('/user/:user/mood', {
        templateUrl: 'mood/mood.html',
        controller: 'MoodCtrl'
      })
      .when('/team/:date?', {
        templateUrl: 'users/team.html',
        controller: 'TeamCtrl'
      })
      .when('/history/:since?/:until?', {
        templateUrl: 'mood/history.html',
        controller: 'HistoryCtrl'
      })
      .when('/user/:user/average', {
        templateUrl: 'stats/user-stats.html',
        controller: 'UserStatsCtrl'
      })
      .when('/day/:day/average', {
        templateUrl: 'stats/day-stats.html',
        controller: 'DayStatsCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });

    Date.prototype.yyyymmdd = function() {
      var yyyy = this.getFullYear().toString();
      var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
      var dd  = this.getDate().toString();
      return yyyy +'-'+ (mm[1]?mm:'0'+mm[0]) +'-'+ (dd[1]?dd:'0'+dd[0]); // padding
    };
  }).controller('NavigarionCtrl', ['$scope', '$location', function ($scope, $location) {
      $scope.isActive = function (page) {
          var currentPage = $location.path();
          return page === currentPage ? 'active' : '';
      };
  }]);
