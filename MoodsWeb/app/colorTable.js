/// <reference path="../../typings/lodash/lodash.d.ts"/>
'use strict';

angular.module('moodsWebApp')
	.factory('ColorTable',function(){
	    var _colorsValues = [
            '10,20,10',
			'220,220,0',
			'0,220,220',
			'220,0,220',
			'220,0,0',
			'0,220,0',
			'0,0,220',
			'120,220,0',
			'220,120,0',
			'0,120,220',
			'0,220,120',
			'220,0,120',
			'120,0,220',
			'110,110,110',
			'110,110,0',
			'0,110,110',
			'110,0,110',
			'110,0,0',
			'0,110,0',
			'0,0,110'
		];
		return {
			colors: function() {
				return _.map(_colorsValues, function (color) {
					return 'rgba('+color+',1)';
				});
			},
			colorsPairs: function() {
				return _.map(_colorsValues, function (color) {
					return {
						color: 'rgba('+color+',1)',
						lightColor: 'rgba('+color+',0.2)'
					}; 
				});
			}
		};
	});