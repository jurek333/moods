﻿'use strict';

angular.module('moodsWebApp')
    .controller('TeamCtrl', ['$scope','$routeParams', 'MoodStorage', 'ColorTable',
        'userService','MoodStats','moodConfig',
        function ($scope, $routeParams, MoodStorage, ColorTable, userService, MoodStats, moodConfig) {
            $scope.options = {
                teamChart: 'teamStats'
            };
            if (undefined === $routeParams.date) {
                $scope.day = moment().format(moodConfig.displayDateFormat);
            } else {
                $scope.day = (moment($routeParams.date, moodConfig.urlDateFormat)).format(moodConfig.displayDateFormat);
            }

            $scope.getPrevDay=function() {
                var current = moment($scope.day, moodConfig.displayDateFormat);
                var prevDate = current.subtract(1,'days');
                return prevDate.format(moodConfig.displayDateFormat);
            };
            $scope.getNextDay = function () {
                var current = moment($scope.day, moodConfig.displayDateFormat);
                var nextDate = current.add(1,'days');
                return nextDate.format(moodConfig.displayDateFormat);
            };

            $scope.printTeamChart = function (moods) {
                var support = _.map(moods, function (item) {
                    return item.name;
                });
                var chartData = _.map(moods, function (item) {
                    return item.average;
                });

                var data = {
                    labels: support,
                    datasets: [
                        {
                            label: 'Zespół',
                            fillColor: 'rgba(20,20,220,0.2)',
                            strokeColor: 'rgba(20,20,220,1)',
                            pointColor: 'rgba(20,20,220,1)',
                            pointStrokeColor: '#fff',
                            pointHighlightFill: '#fff',
                            pointHighlightStroke: 'rgba(20,20,220,1)',
                            data: chartData
                        }
                    ]
                };
                var ctx = document.getElementById($scope.options.teamChart).getContext("2d");
                $scope.teamChart = new Chart(ctx).Radar(data);
            };

            $scope.getUserArray = function (historyData, userList) {
                var users = [];
                var groupedByUser = _.groupBy(historyData, function (item) {
                    return item.userId;
                });
                _.forEach(groupedByUser, function (user, id) {
                    var userInfo = _.find(userList, 'userId', parseInt(id));

                    if (user.length > 0 && undefined !== userInfo) {
                        var value = _.sum(user, 'moodValue') / user.length;

                        users.push({
                            id: id,
                            name: userInfo.name,
                            average: Math.round( value * 100) / 100
                        });
                    }
                });
                return users;
            };
            userService.users()
                .success(function(data){
                    $scope.users = data;

                    MoodStorage.history($scope.day, $scope.day)
                        .success(function (data, status, headers, config) {
                            if (data.length === 0) {
                                return;
                            }
                            var analysed = $scope.getUserArray(data, $scope.users);
                            $scope.printTeamChart(analysed);
                        });
                });

            MoodStats.dayAverage($scope.day)
                .success(function (data, status, headers, config) {
                    var averageValue = 0;
                    if (undefined !== data && 0 < data.length) {
                        averageValue = Math.round(100 * data[0].average) / 100;
                    }
                    $scope.avg = [averageValue];
                });
        }]);
