'use strict';

angular.module('moodsWebApp')
	.controller('UsersCtrl', ['$scope','userService',
	function($scope, userService){
		userService.users()
			.success(function(data){
				$scope.dictionary = {
		    		users: data 
				};
			});
	}]);