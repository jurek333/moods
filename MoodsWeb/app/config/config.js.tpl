'use strict';

angular.module('moodsWebApp.config', [])
.constant('moodConfig', {
  "backend": '<%- backend %>',
  "moods": [{
    "name": 'Radosna<br/><b>Kuoka</b>',
    "value": 5,
    "img": 'images/kuoka.png'
  },{
    "name": 'Uśmiechnięty<br/><b>Koala</b>',
    "value": 4,
    "img": 'images/koala.png'
  },{
    "name": 'Amorficzna<br/><b>Ameba</b>',
    "value": 3,
    "img": 'images/ameba.png'
  },{
    "name": 'Radioaktywna<br/><b>Koza</b>',
    "value": 2,
    "img": 'images/koza.gif'
  },{
    "name": 'Wkur(.*{1,2})ony<br/><b>Jeż</b>',
    "value": 1,
    "img": 'images/jez.png'
  }],
  "team": 1,
  "displayDateFormat": 'YYYY-MM-DD',
  "urlDateFormat": 'YYYY-MM-DD'
});
