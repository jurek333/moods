'use strict';

angular.module('moodsWebApp')
	.controller('UserStatsCtrl', function($scope, $routeParams, MoodStats, userService) {
		
		$scope.user = {
			id: parseInt($routeParams.user),
			name: ''
		};
		userService.user($scope.user.id)
			.success(function(data) {
				$scope.user.name = data.name;
			});
		
		MoodStats.userAverage($scope.user.id)
			.success(function (data, status, headers, config) {
			    if (data !== undefined) {
			        $scope.avg = [data.average];
			    }
			});
	});