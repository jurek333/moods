'use strict';

angular.module('moodsWebApp')
	.service('MoodStorage', function($http, Notification, moodConfig) {
		var baseUrl = moodConfig.backend;
		this.save = function(mood) {
			return $http.post(baseUrl+'/mood/', mood)
				.error(function(data, status, headers, config){
					Notification.error({
						message: 'Error notification', 
						title: 'Error'
					});
				});
		}
		this.history = function(since, until) {
			return $http.get(baseUrl+'/mood/'+since+'/'+until)
				.error(function(data, status, headers, config){
					Notification.error({
						message: 'Nie udało się pobrać historii', 
						title: 'Błąd'
					});
				});
		}
	});