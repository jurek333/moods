/* global Notification */
'use strict';

angular.module('moodsWebApp')
	.service('MoodStats', function($http, moodConfig) {
		var baseUrl = moodConfig.backend;
		this.userAverage = function(user) {
			return $http.get(baseUrl+'/stats/user/'+user)				 
				 .error(function(data,status,headers,config){
				 	Notification.error({
						message: 'Nie udało się pobrać średniego wyniku członka zespołu', 
						title: 'Błąd'
					});
				 });
		};
		this.dayAverage = function(day) {
			return $http.get(baseUrl+'/stats/days?since='+day+'&until='+day)
				 .error(function(data,status,headers,config){
				 	Notification.error({
						message: 'Nie uda�o si� pobra� informacji o �rednim wyniku', 
						title: 'B��d'
					});
				 });
		};
		this.daysAverage = function (since, until) {
		    return $http.get(baseUrl + '/stats/days?since=' + since + '&until=' + until)
				 .error(function (data, status, headers, config) {
				     Notification.error({
				         message: 'Nie uda�o si� pobra� �rednich warto�ci za ��dany okres',
				         title: 'B��d'
				     });
				 });
		};
	});