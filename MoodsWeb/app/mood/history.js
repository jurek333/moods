﻿/* global Chart */
'use strict';

angular.module('moodsWebApp')
    .controller('HistoryCtrl', function ($scope, $routeParams, MoodStats, MoodStorage, userService, moodConfig, ColorTable, $sce) {

    if (undefined === $routeParams.until) {
        $scope.until = new Date().yyyymmdd();
    } else {
        $scope.until = (new Date($routeParams.until)).yyyymmdd();
    }
    if (undefined === $routeParams.since) {
        $scope.since = new Date((new Date()).setDate(new Date($scope.until).getDate() - 14)).yyyymmdd();
    } else {
        $scope.since = (new Date($routeParams.since)).yyyymmdd();
    }
    if (new Date($scope.until) < new Date($scope.since)) {
        var tmp = $scope.until;
        $scope.until = $scope.since;
        $scope.since = tmp;
    }

    $scope.moods = moodConfig.moods;
    userService.users().success(function (data) {
        $scope.users = data;
        showHistory();
    });
    $scope.options = {
        chartId: 'averageMoodChart'
    };

    $scope.printLegend = function () {
        if (undefined === $scope.chart) {
            return '';
        }
        var html = $scope.chart.generateLegend();
        return $sce.trustAsHtml(html);
    };

    $scope.printChart = function () {
        if (undefined !== $scope.chart) {
            $scope.chart.destroy();
            $scope.chart = undefined;
        }
        var ctx = document.getElementById($scope.options.chartId).getContext('2d');
        var stats = {
            labels: $scope.chartSupport,
            datasets: []
        };

        _.forEach($scope.usersDatasets, function (set) {
            stats.datasets.push(set);
        });
        if (null !== $scope.averageDataset) {
            stats.datasets.push($scope.averageDataset);
        }

        $scope.chart = new Chart(ctx).Line(stats, {
            legendTemplate: '<div class="btn-group-vertical <%=name.toLowerCase()%>-legend" role="group"><% for (var i=0; i<datasets.length; i++){%><a style="background-color:<%=datasets[i].fillColor%>;color:<%=datasets[i].strokeColor%>" class="btn btn-default"><%if(datasets[i].label){%><%=datasets[i].label%><%}%></a><%}%>'
        });
    };

    $scope.getUserArray = function (data) {
        var users = [];
        var groupedByUser = _.groupBy(data, function (item) {
            return item.userId;
        });
        _.forEach(groupedByUser, function (user) {
            if (user.length > 0) {
              var userName = _.result(_.find($scope.users, 'userId', _.get(user, '0').userId), 'name', '');
              if('' !== userName) {
                users.push({
                  id: _.get(user, '0').userId,
                  name: userName,
                  data: []
                });
              }
            }
        });
        return users;
    };

    function showHistory() {
        MoodStorage.history($scope.since, $scope.until)
            .success(function (data, status, headers, config) {
            $scope.usersDatasets = [];
            if (0 < data.length) {
                var userData = $scope.getUserArray(data);
                var grByDay = _.groupBy(_.sortBy(data, 'day'), function (n) {
                    return n.day;
                });

                _.forEach(grByDay, function (dayData) {
                    if (dayData.length > 0) {
                        var byUser = _.groupBy(dayData, function (v) {
                            return v.userId;
                        });
                        _.forEach(userData, function (user) {
                            var agr = _.get(byUser, user.id + '');
                            if (undefined === agr || agr.length === 0) {
                                user.data.push(0);
                            } else {
                                var srednia = Math.round(100 * _.sum(agr, 'moodValue') / agr.length) / 100;
                                user.data.push(srednia);
                            }
                        });
                    }
                });

                var fColor = {};
                var colors = ColorTable.colorsPairs();
                var count = 1, index = 0;
                _.forEach(userData, function (user) {
                    index = count % colors.length;
                    fColor = colors[count];
                    count = count + 1;
                    $scope.usersDatasets.push({
                        label: user.name,
                        //fillColor: fColor.lightColor,
                        fillColor: 'rgba(0,0,0,0)',
                        strokeColor: fColor.color,
                        pointColor: fColor.color,
                        pointStrokeColor: '#fff',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: fColor.color,
                        data: user.data
                    });
                });
            }
            if (undefined !== $scope.chartSupport && $scope.chartSupport.length > 0) {
                $scope.printChart();
            }
        });
    }

    MoodStats.daysAverage($scope.since, $scope.until)
        .success(function (data, status, headers, config) {
        $scope.averageDataset = null;
        $scope.chartSupport = [];
        if (0 < data.length) {
            var dataSortedByDay = _.sortBy(data, 'day');

            var fColor = ColorTable.colorsPairs()[0];

            var averageDatas = {
                label: 'Średnia',
                fillColor: 'rgba(220,220,220,0.0)',
                strokeColor: fColor.color,//'rgba('+fColor+',1)',
                pointColor: fColor.color,//'rgba('+fColor+',1)',
                pointStrokeColor: '#fff',
                pointHighlightFill: '#fff',
                pointHighlightStroke: fColor.color,//'rgba('+fColor+',1)',
                data: _.map(dataSortedByDay, function (item) {
                    return Math.round(100 * item.average) / 100;
                })
            };

            $scope.averageDataset = averageDatas;
            $scope.chartSupport = _.map(dataSortedByDay, function (d) {
                return new Date(d.day).yyyymmdd();
            });
        }
        if (undefined !== $scope.usersDatasets && $scope.usersDatasets.length > 0) {
            $scope.printChart();
        }
    });
});
