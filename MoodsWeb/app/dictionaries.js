/// <reference path="../../typings/lodash/lodash.d.ts"/>
'use strict';

angular.module('moodsWebApp')
	.service('userService', 
	function($http, Notification, moodConfig){
		this.users = function(){
			var url = moodConfig.backend + '/user';
			if(undefined !== moodConfig.team){
				url = url + '/team/'+ moodConfig.team;
			}
			return $http.get(url)
					.error(function(){
						Notification.error({
							message: 'Nie udało się pobrać użytkowników', 
							title: 'Błąd'
						});
					});
		};
		this.user = function(id) {
			var url = moodConfig.backend + '/user/' + id;
			return $http.get(url)
					.error(function(){
						Notification.error({
							message: 'Nie udało się pobrać użytkowników', 
							title: 'Błąd'
						});
					});
		};
	});